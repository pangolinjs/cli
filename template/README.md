# Pangolin.js

Framework for design system development with Nunjucks, Sass, and JavaScript.

## Requirements

* [Node.js 14+](https://nodejs.org)

## Install dependencies

```bash
npm install
```

## Tasks

### Development

```bash
npm run dev
```

### Linting

```bash
npm run lint:css
npm run lint:js
```

### Build

Generate production-ready files for CMS integration (output to `dist`) and static design system export (output to `static`).

```bash
npm run build
```

Take a look at the [full documentation for Pangolin.js](https://pangolinjs.org).
